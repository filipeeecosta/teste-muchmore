mapa = {
  
  init : function(id, markers, lat, long){

    var latlng  = new google.maps.LatLng(lat, long);

    var options = {
      scrollwheel : false,
      zoom    : 5,
      center    : latlng,
      mapTypeId : google.maps.MapTypeId.ROADMAP
    };

    var map = new google.maps.Map(document.getElementById("mapa_" + id), options);

    mapa.displayMarkers(map, markers, latlng);
  },

  displayMarkers : function (map, markersData, latlng){

    var bounds = new google.maps.LatLngBounds();

    for (var i = 0; i < markersData.length; i++){

      var latlng    = new google.maps.LatLng(markersData[i].lat, markersData[i].lng);
      var nome    = markersData[i].nome;

      mapa.createMarker(map, latlng, nome);

      bounds.extend(latlng); 
    }

    map.fitBounds(bounds);
  },

  createMarker : function(map, latlng, nome){

    var marker = new google.maps.Marker({
      map: map,
      position: latlng,
      title: nome
    });

    google.maps.event.addListener(marker, 'click', function() {

      infoWindow.setContent(iwContent);

      infoWindow.open(map, marker);
    });
  }

};


  APP = {

    buscar_revendas : function( page ) {

      var offset = page - 1;

      if ( page < 0 ) {
        offset = 0;
      }

      $('.panel-group').hide();

      $.ajax({
        url: "https://api.services.mourafacil.com.br/api/v1/revenda",
        data: {
          limit: 20,
          offset: offset
        },
        type: "GET",

      }).done(function( json ) {

        APP.definir_paginacao( json.total, page+1 );

        APP.exibir_resultado_busca(json.revendas);

      }).fail(function( xhr, status, errorThrown ) {

        APP.exibir_feedback_erro();

      });
    },
    exibir_resultado_busca : function( lista ) {

      var html = '';

      $.each( lista, function( i, revenda ) {

         html += '<div class="panel panel-default">';
           html += '<div class="panel-heading" role="tab" id="headingOne">';
             html += '<h4 class="panel-title">';
               html += '<a role="button" data-toggle="collapse" data-parent="#accordion" href="#revenda_'+ revenda.id +'" aria-expanded="true" aria-controls="revenda_'+ revenda.id +'">';
                  html += revenda.nome;
               html += '</a>';
             html += '</h4>';
           html += '</div>';
           html += '<div id="revenda_'+ revenda.id +'" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">';
             html += '<div class="panel-body">';
               html += '<div class="row">';
                 html += '<div class="col-md-4">';
                   html += '<ul class="list-group">';
                     html += '<li class="list-group-item"><strong>Revenda: </strong> '+ revenda.nome +'</li>';
                     html += '<li class="list-group-item"><strong>Status: </strong> '+ revenda.status +'</li>';
                     html += '<li class="list-group-item"><strong>Logradouro: </strong> '+ revenda.endereco.logradouro +'</li>';
                     html += '<li class="list-group-item"><strong>Número: </strong> '+ revenda.endereco.numero +'</li>';
                     html += '<li class="list-group-item"><strong>Complemento: </strong> '+ revenda.endereco.complemento +'</li>';
                     html += '<li class="list-group-item"><strong>Cep: </strong> '+ revenda.endereco.cep +'</li>';
                     html += '<li class="list-group-item"><strong>Bairro: </strong> '+ revenda.endereco.bairro +'</li>';
                     html += '<li class="list-group-item"><strong>Cidade: </strong> '+ revenda.endereco.cidade +'</li>';
                     html += '<li class="list-group-item"><strong>E-mail: </strong> '+ revenda.email +'</li>';
                     html += '<li class="list-group-item"><strong>Telefone: </strong> '+ revenda.telefone +'</li>';
                   html += '</ul>';
                 html += '</div>';
                 html += '<div class="col-md-8">';
                   html += '<div class="mapa" id="mapa_'+ revenda.id +'"></div>';
                 html += '</div>';
               html += '</div>';
             html += '</div>';
           html += '</div>';
         html += '</div>';

      });

      $('.panel-group').html( html );
      $('.panel-group').show();

      $.each( lista, function( i, revenda ) {


        var markers = [
           {
             lat: revenda.lat,
             lng: revenda.lng,
             nome: revenda.nome,
           }
        ];

        mapa.init(revenda.id, markers, revenda.lat, revenda.lng);
      });
      
    }, 
    exibir_feedback_erro : function() {
      $('h2').after('<div class="alert alert-danger" >Problema ao buscar revendas</div>');
    },
    definir_paginacao : function ( total, page ) {

        var defaultOptions = {
          totalPages : total,
          currentPage : page,
          first : 'Primeira',
          prev : 'Anterior',
          next : 'Próxima',
          last : 'Última',
          onPageClick: function (evt, page) {
            APP.buscar_revendas( page ); 
          }
        };

        pagination = $('#paginacao');

        pagination.twbsPagination( defaultOptions );

    }
  };

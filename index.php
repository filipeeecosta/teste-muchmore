<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>Teste Much More</title>

  <!-- Bootstrap -->
  <link href="css/style.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>

    <header class="topo">
      <h1>Desafio Much More</h1>
    </header>

    <div class="container">

      <h2>Lista de revendas</h2>

      <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true"></div>
      <!-- lista de revendas -->
      
      <ul id="paginacao" class="pagination-sm"></ul>
      <!-- paginação -->

    </div>

    <footer class="rodape">
      <address>@copyright 2018</address>
    </footer>

    <script src="js/lib/jquery.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD4S4zSbKWXHsPUX0KHmi48iQwvQEIkgW8"></script>
    <script src="js/lib/bootstrap.min.js"></script>
    <script src="js/lib/jquery.twbsPagination.min.js"></script>
    <script src="js/app/models/mapa.js"></script>
    <script src="js/app/models/app.js"></script>
    <script src="js/app/controllers/main.js"></script>

  </body>
  </html>